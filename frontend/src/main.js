import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'

import Delivery from './components/Delivery.vue';
import Payment from './components/Payment.vue';
import Finish from './components/Finish.vue';

Vue.use(VueRouter);

const routers = [{
  name: 'Delivery',
  path: '/delivery',
  component: Delivery
},
{
  name: 'Payment',
  path: '/payment',
  component: Payment
},
{
  name: 'Finish',
  path: '/finish',
  component: Finish
}
]

const router = new VueRouter({mode:'history', routes: routers})

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
